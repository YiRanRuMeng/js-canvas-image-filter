export default {
    //黑白调 
    //原理：判断当前像素的RGB值是否大于255的一半,如大于就全部设置为255,小于就全部设为0
    blackWhite(imageData) {
        //所在区域图片的像素集
        let data = imageData.data;
        for (let i = 0; i < data.length; i += 4) {
            let r = data[i];
            let g = data[i + 1];
            let b = data[i + 2];
            if (r > 255 / 2) {
                data[i] = 255
                data[i + 1] = 255
                data[i + 2] = 255
            } else if (r < 255 / 2) {
                data[i] = 0
                data[i + 1] = 0
                data[i + 2] = 0
            }
        }
    },
    //灰色调
    //原理：把当前像素的RGB值 设置为当前像素RGB的平均值
    gray(imageData) {
        let data = imageData.data;
        for (let i = 0; i < data.length; i += 4) {
            let r = data[i];
            let g = data[i + 1];
            let b = data[i + 2];

            let average = Math.floor((r + g + b) / 3);
            data[i] = average;
            data[i + 1] = average;
            data[i + 2] = average;
        }
    },

    //反转 
    //原理：用255 - 当前像素的RGB的值
    toggle(imageData) {
        let data = imageData.data;
        for (let i = 0, len = data.length; i < len; i += 4) {
            data[i] = 255 - data[i];
            data[i + 1] = 255 - data[i + 1]
            data[i + 2] = 255 - data[i + 2];
        }
    },
    //复古
    //原理：RGB值乘以固定的数值
    sepia(imageData) {
        let data = imageData.data;
        for (let i = 0; i < data.length; i += 4) {
            let r = data[i];
            let g = data[i + 1];
            let b = data[i + 2];
            let rs = 0.393;
            let gs = 0.769;
            let bs = 0.189;
            data[i] = (r * rs) + (g * gs) + (b * bs);
        }
    },
    // 红色蒙版
    // 原理：红色通道取平均值,绿色通道和蓝色通道都设为0
    myRed(imageData) {
        let data = imageData.data;
        for (let i = 0; i < data.length; i += 4) {
            let r = data[i];
            let g = data[i + 1];
            let b = data[i + 2];
            data[i] = (r + g + b) / 3;
            data[i + 1] = 0;
            data[i + 2] = 0;
        }
    },
    //增加亮度
    //原理：RGB值直接加上所需要设置亮度delta
    brightness(imageData, delta) {
        let data = imageData.data;
        for (let i = 0; i < data.length; i += 4) {
            data[i] += delta;
            data[i + 1] += delta;
            data[i + 2] += delta;
        }
    },
    //浮雕
    //原理：每个像素的RGB值都设置为该位置的初始值 num 减去其上一个像素值得差，最后统一加上128用于控制灰度
    carve(imageData) {
        let w = imageData.width
        let h = imageData.height;
        let data = imageData.data;
        for (let i = h; i > 0; i--) {  // 行
            for (let j = w; j > 0; j--) {  // 列
                for (let k = 0; k < 3; k++) {
                    let num = (i * w + j) * 4 + k;
                    let numUp = ((i - 1) * w + j) * 4 + k;
                    let numDown = ((i + 1) * w + j) * 4 + k;
                    data[num] = data[num] - data[numUp - 4] + 128;
                }
            }
        }
    },

    // 雾化
    // 原理：像素点周围随机设置255白色值
    fog(imageData) {
        let w = imageData.width
        let h = imageData.height;
        let data = imageData.data;
        for (let i = h; i > 0; i--) {  // 行
            for (let j = w; j > 0; j--) {  // 列
                let num = (i * w + j) * 4;
                if (Math.random() < 0.1) {
                    data[num] = 255;
                    data[num + 1] = 255;
                    data[num + 2] = 255;
                }
            }
        }
    },

    // 毛玻璃效果
    // 原理：用当前点四周一定范围内任意一点的颜色来替代当前点颜色，最常用的是随机的采用相邻点进行替代。
    spread(canvasData) {
        let w = canvasData.width
        let h = canvasData.height;
        for (let i = 0; i < h; i++) {
            for (let j = 0; j < w; j++) {
                for (let k = 0; k < 3; k++) {
                    // Index of the pixel in the array  
                    let num = (i * w + j) * 4 + k;
                    let rand = Math.floor(Math.random() * 10) % 3;
                    let num2 = ((i + rand) * w + (j + rand)) * 4 + k;
                    canvasData.data[num] = canvasData.data[num2]
                }
            }
        }
    },
    // 马赛克
    // 将图像分成大小一致的图像块，每一个图像块都是一个正方形，
    // 并且在这个正方形中所有像素值都相等。我们可以将这个正方形看作是一个模板窗口，
    // 模板中对应的所有图像像素值都等于该模板的左上角第一个像素的像素值，
    // 这样的效果就是马赛克效果，而正方形模板的大小则决定了马赛克块的大小，即图像马赛克化的程度。
    mosaic(imageData, size) {
        let w = imageData.width
        let h = imageData.height;
        let data = imageData.data;
        for (let i = 1; i < h - 1; i += size) {
            for (let j = 1; j < w - 1; j += size) {
                let num = (i * w + j) * 4;
                for (let dx = 0; dx < size; dx++) {
                    for (let dy = 0; dy < size; dy++) {
                        let x = i + dx;
                        let y = j + dy;
                        let p1 = (x * w + y) * 4;

                        data[p1 + 0] = data[num + 0];
                        data[p1 + 1] = data[num + 1];
                        data[p1 + 2] = data[num + 2];
                    }
                }
            }
        }
    },
    //模糊滤镜 
    //原理：将当前像素的周边像素的RGB值各自的平均值作为新的RGB值。
    myBlur(imageData) {
        let w = imageData.width
        let h = imageData.height;
        let data1 = imageData.data;
        let data2 = imageData.data;

        for (let i = 0; i < h; i++) {  // 行
            for (let j = 0; j < w; j++) {  // 列
                for (let k = 0; k < 3; k++) {
                    let num = (i * w + j) * 4 + k;
                    let numUp = ((i - 1) * w + j) * 4 + k;
                    let numDown = ((i + 1) * w + j) * 4 + k;
                    // 对另开内存的data1的改变为什么会反应到data中
                    data1[num] = (data2[numUp - 4] + data2[numUp] + data2[numUp + 4]
                        + data2[num - 4] + data2[num] + data2[num + 4]
                        + data2[numDown - 4] + data2[numDown] + data2[numDown + 4]) / 9;
                }
            }
        }
    }
}
