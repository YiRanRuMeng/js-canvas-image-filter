

## 效果展示

![](./images/demo.gif)

## 实现思路

我们知道每张图片都是由若干像素组成，得到的像素是一个数组，颜色又是由RGBA组成，所以数组中每4个点组成一个颜色值，要去实现每个滤镜的特效,就要去有规律的去改变像素值。当我们拿到图片并且通过ctx.drawImage()方法绘制到cavans中后，可以通过ctx.getImageData()方法来获取图片数据。然后就可以通过filter.js来调用方法实现滤镜效果。

## cavans前置准备

### 1.获取cavans 
```javascript
let filterCavans = document.getElementById("filterCavans");
filterCavans.width = img.clientWidth;
filterCavans.height = img.clientHeight;
```
### 2.获取2d context对象
```javascript
ctx = filterCavans.getContext("2d");
```
### 3.绘制图片到cavans上
```javascript
let img = document.getElementById("img");
ctx.drawImage(img, 0, 0, img.clientWidth, img.clientHeight);
```
### 4.获取在cavans上已绘制图片数据
```javascript
canvasData = ctx.getImageData( 0, 0, filterCavans.width, filterCavans.height);
```
## 原理及实现

### 1.黑白调

原理：判断当前像素的RGB值是否大于255的一半,如大于就全部设置为255,小于就全部设为0
```javascript
blackWhite(imageData) {
    //所在区域图片的像素集
    let data = imageData.data;
    for (let i = 0; i < data.length; i += 4) {
        let r = data[i];
        let g = data[i + 1];
        let b = data[i + 2];
        if (r > 255 / 2) {
            data[i] = 255
            data[i + 1] = 255
            data[i + 2] = 255
        } else if (r < 255 / 2) {
            data[i] = 0
            data[i + 1] = 0
            data[i + 2] = 0
        }
    }
}

```
### 2.灰色调
原理：把当前像素的RGB值 设置为当前像素RGB的平均值
```javascript
gray(imageData) {
    let data = imageData.data;
    for (let i = 0; i < data.length; i += 4) {
        let r = data[i];
        let g = data[i + 1];
        let b = data[i + 2];

        let average = Math.floor((r + g + b) / 3);
        data[i] = average;
        data[i + 1] = average;
        data[i + 2] = average;
    }
}
```
### 3.反转
原理：用255减去当前像素的RGB值
```javascript

toggle(imageData) {
    let data = imageData.data;
    for (let i = 0, len = data.length; i < len; i += 4) {
        data[i] = 255 - data[i];
        data[i + 1] = 255 - data[i + 1]
        data[i + 2] = 255 - data[i + 2];
    }
}
```
### 4.复古
原理：RGB值乘以固定的数值
```javascript

sepia(imageData) {
    let data = imageData.data;
    for (let i = 0; i < data.length; i += 4) {
        let r = data[i];
        let g = data[i + 1];
        let b = data[i + 2];
        data[i] = (r * 0.393) + (g * 0.769) + (b * 0.189);
    }
}
```
### 5.红色蒙版

原理：红色通道取平均值,绿色通道和蓝色通道都设为0
```javascript
myRed(imageData) {
    let data = imageData.data;
    for (let i = 0; i < data.length; i += 4) {
        let r = data[i];
        let g = data[i + 1];
        let b = data[i + 2];
        data[i] = (r + g + b) / 3;
        data[i + 1] = 0;
        data[i + 2] = 0;
    }
}
```
### 6.增加亮度

原理：RGB值直接加上所需要设置亮度delta
```javascript
brightness(imageData, delta) {
    let data = imageData.data;
    for (let i = 0; i < data.length; i += 4) {
        data[i] += delta;
        data[i + 1] += delta;
        data[i + 2] += delta;
    }
}
```
### 7.浮雕

原理：每个像素的RGB值都设置为该位置的初始值 num 减去其上一个像素值得差，最后统一加上128用于控制灰度
```javascript
carve(imageData) {
    let w = imageData.width, h = imageData.height;
    let data = imageData.data;
    for (let i = h; i > 0; i--) {  // 行
        for (let j = w; j > 0; j--) {  // 列
            for (let k = 0; k < 3; k++) {
                let num = (i * w + j) * 4 + k;
                let numUp = ((i - 1) * w + j) * 4 + k;
                let numDown = ((i + 1) * w + j) * 4 + k;
                data[num] = data[num] - data[numUp - 4] + 128;
            }
        }
    }
}
```
### 8.雾化

原理：通过随机方法来设置当前像素点周围的255白色值
```javascript
fog(imageData) {
    let w = imageData.width, h = imageData.height;
    let data = imageData.data;
    for (let i = h; i > 0; i--) {  // 行
        for (let j = w; j > 0; j--) {  // 列
            let num = (i * w + j) * 4;
            if (Math.random() < 0.1) {
                data[num] = 255;
                data[num + 1] = 255;
                data[num + 2] = 255;
            }
        }
    }
}
```
### 9.毛玻璃

原理：用当前点四周一定范围内任意一点的颜色来替代当前点颜色，最常用的是随机的采用相邻点进行替代。
```javascript
spread(canvasData) {
    let w = canvasData.width, h = canvasData.height;
    for (let i = 0; i < h; i++) {
        for (let j = 0; j < w; j++) {
            for (let k = 0; k < 3; k++) {
                // Index of the pixel in the array  
                let num = (i * w + j) * 4 + k;
                let rand = Math.floor(Math.random() * 10) % 3;
                let num2 = ((i + rand) * w + (j + rand)) * 4 + k;
                canvasData.data[num] = canvasData.data[num2]
            }
        }
    }
}
```
### 10.马赛克

原理：将图像分成大小一致的图像块，每一个图像块都是一个正方形，并且在这个正方形中所有像素值都相等。我们可以将这个正方形看作是一个模板窗口，模板中对应的所有图像像素值都等于该模板的左上角第一个像素的像素值，这样的效果就是马赛克效果，而正方形模板的大小则决定了马赛克块的大小，即图像马赛克化的程度。
```javascript
mosaic(imageData, size) {
    let w = imageData.width, h = imageData.height;
    let data = imageData.data;
    for (let i = 1; i < h - 1; i += size) {
        for (let j = 1; j < w - 1; j += size) {
            let num = (i * w + j) * 4;
            for (let dx = 0; dx < size; dx++) {
                for (let dy = 0; dy < size; dy++) {
                    let x = i + dx;
                    let y = j + dy;
                    let p1 = (x * w + y) * 4;

                    data[p1 + 0] = data[num + 0];
                    data[p1 + 1] = data[num + 1];
                    data[p1 + 2] = data[num + 2];
                }
            }
        }
    }
}
```
### 11.模糊

原理：将当前像素的周边像素的RGB值各自的平均值作为新的RGB值
```javascript
myBlur(imageData) {
    let w = imageData.width, h = imageData.height;
    let data1 = imageData.data;
    let data2 = imageData.data;

    for (let i = 0; i < h; i++) {  // 行
        for (let j = 0; j < w; j++) {  // 列
            for (let k = 0; k < 3; k++) {
                let num = (i * w + j) * 4 + k;
                let numUp = ((i - 1) * w + j) * 4 + k;
                let numDown = ((i + 1) * w + j) * 4 + k;
                // 对另开内存的data1的改变为什么会反应到data中
                data1[num] = (data2[numUp - 4] + data2[numUp] + data2[numUp + 4]
                    + data2[num - 4] + data2[num] + data2[num + 4]
                    + data2[numDown - 4] + data2[numDown] + data2[numDown + 4]) / 9;
            }
        }
    }
}
```
## 使用
```javascript
//黑白调
filter.blackWhite(canvasData);

//保存图片
save() {
    this.download("png");
},
//利用a标签下载
download(type) {
    //设置保存图片的类型
    let imgdata = filterCavans.toDataURL(type);
    //将mime-type改为image/octet-stream,强制让浏览器下载
    let fixtype = function (type) {
        type = type.toLocaleLowerCase().replace(/jpg/i, "jpeg");
        let r = type.match(/png|jpeg|bmp|gif/)[0];
        return "image/" + r;
    };
    imgdata = imgdata.replace(fixtype(type), "image/octet-stream");
    //将图片保存到本地
    let saveFile = function (data, filename) {
        let link = document.createElement("a");
        link.href = data;
        link.download = filename;
        link.click();
    };
    let filename = new Date().toLocaleDateString() + "." + type;
    saveFile(imgdata, filename);
}
```
